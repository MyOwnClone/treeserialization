#include <string>
#include <fstream>
#include <map>
#include <vector>
#include "tree.h"

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/vector.hpp>

struct intVariable
{
	std::string name;
	int			value;
	int			id;

	static int counter;

	intVariable()
	{
		value = 0;
	}

	intVariable(const intVariable& orig)
	{
		id = orig.id;
		name = orig.name;
		value = orig.value;
	}

	intVariable(std::string name, int value)
	{
		this->name = name;
		this->value = value;

		id = counter++;
	}

	intVariable(int id, std::string name, int value)
	{
		this->id = id;
		this->name = name;
		this->value = value;
	}

	bool operator==(const intVariable& other) const
	{
		return (name == other.name && value == other.value);
	}

	
	friend class boost::serialization::access;
	template<class Archive>	void serialize(Archive & ar, const unsigned int version)
	{
		ar & id;
		ar & name;
		ar & value;
	}
};

int intVariable::counter = 0;

core::tree<intVariable> loadTree(boost::archive::text_iarchive &treeIta, boost::archive::text_iarchive &vecIta);
void printTree(core::tree<intVariable>::iterator iter);
void saveTree(core::tree<intVariable>::iterator iter, std::vector<intVariable> &items, std::map<int, std::vector<int>> &mapping);

void printTree(core::tree<intVariable>::iterator iter)
{
	for (auto innerIter = iter.begin(); innerIter != iter.end(); innerIter++)
	{
		std::cout << innerIter.level() << "\t" << innerIter.data().id << "\t" << innerIter.data().name << std::endl;

		printTree(innerIter);
	}
}

void saveTree(core::tree<intVariable>::iterator iter, std::vector<intVariable> &items, std::map<int, std::vector<int>> &mapping)
{
	for (auto innerIter = iter.begin(); innerIter != iter.end(); innerIter++)
	{
		if (mapping.find(iter.data().id) == mapping.end())
		{
			mapping.insert(std::pair<int, std::vector<int>>(iter.data().id, std::vector<int>()));
		}

		mapping[iter.data().id].push_back(innerIter.data().id);

		items.push_back(innerIter.data());

		saveTree(innerIter, items, mapping);
	}
}

core::tree<intVariable> loadTree(boost::archive::text_iarchive &treeIta, boost::archive::text_iarchive &vecIta)
{
	core::tree<intVariable> result;

	std::vector<intVariable> items;
	std::map<int, intVariable> idDict;

	treeIta >> items;
	for (auto i = 0; i < items.size(); i++)
	{
		idDict.insert(std::make_pair(items[i].id, items[i]));
	}

	std::map<int, std::vector<int>> mapping;

	try {
		vecIta >> mapping;
	}
	catch (boost::archive::archive_exception e)
	{
		std::cout << "input file operation failed" << std::endl;
		return result;
	}

	auto rootCreated = false;
	core::tree<intVariable>::iterator currentNode = result;

	std::map<int, core::tree<intVariable>::iterator> nodesCreated;

	for each (auto var in mapping)
	{
		auto id = var.first;
		auto children = var.second;
		auto parent = idDict[id];

		if (!rootCreated)
		{
			*currentNode = parent;
			rootCreated = true;
		}
		else
		{
			if (nodesCreated.find(id) != nodesCreated.end())
			{
				currentNode = nodesCreated.find(id)->second;
			}
		}

		for (auto i = 0; i < children.size(); i++)
		{
			auto childId = children[i];
			auto child = idDict[childId];

			nodesCreated.insert(std::make_pair(child.id, currentNode.push_back(child)));
		}		
	}

	return result;
}

int main()
{
	{
		std::map<int, std::vector<int>> mapping;

		core::tree<intVariable> tree;
		*tree = intVariable("root", 0);

		auto iter1 = tree.push_back(intVariable("10", 1));
		auto iter2 = tree.push_back(intVariable("11", 2));

		iter1.push_back(intVariable("20", 3));
		iter1.push_back(intVariable("21", 4));

		iter2.push_back(intVariable("22", 5));
		iter2.push_back(intVariable("23", 6));

		std::cout << "original tree:" << std::endl;

		printTree(tree);

		std::ofstream treeOutputFileStream("tree");
		boost::archive::text_oarchive treeOutputArchiver(treeOutputFileStream);
		std::vector<intVariable> treeItems;
		saveTree(tree, treeItems, mapping);

		treeOutputArchiver << const_cast<const std::vector<intVariable>&>(treeItems);

		std::ofstream vectorOutputFileStream("vec");
		boost::archive::text_oarchive vectorOutputArchiver(vectorOutputFileStream);
		vectorOutputArchiver << const_cast<const std::map<int, std::vector<int>>&>(mapping);
	}

	{
		std::ifstream treeInputStream("tree");
		std::ifstream vectorInputStream("vec");
		boost::archive::text_iarchive treeInputArchiver(treeInputStream);
		boost::archive::text_iarchive vectorInputArchiver(vectorInputStream);

		auto loadedTree = loadTree(treeInputArchiver, vectorInputArchiver);

		std::cout << "loaded tree:" << std::endl;

		printTree(loadedTree);
	}
}